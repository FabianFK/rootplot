from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'blue': (68,119,170),
        'cyan': (102,204,238),
        'green': (34,136,51),
        'yellow': (204,187,68),
        'red': (238,102,119),
        'purple': (170,51,119),
        'grey': (187,187,187),
    },
    order = [ 'blue', 'red', 'green', 'yellow', 'cyan', 'purple', 'grey']
    )

apply = lambda *a: _apply_colors(stylecolors[:-1], *a)

del generate_colors
