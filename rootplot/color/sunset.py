from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (54,75,154),
    (74,123,183),
    (110,166,205),
    (152,202,225),
    (194,228,239),
    (234,236,204),
    (254,218,139),
    (253,179,102),
    (246,126,75),
    (221,61,45),
    (165,0,38)
    ]

names = [ 'blue1', 'blue2', 'blue3', 'blue4', 'paleblue', 'palegreen', 'yellow', 'orange1', 'orange2', 'red', 'darkred' ]

generate_colors(
    locals(),
    dict(list(zip(names, rgbs)) + [('white', (255,255,255))]),
    order = names
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

_palette_start = None
def set_palette():
    global _palette_start
    _palette_start = _set_palette(_palette_start, rgbs, 255)

del generate_colors
