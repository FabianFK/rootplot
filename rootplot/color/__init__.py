from ._make_color import float_rgb as _float_rgb
import ROOT as _R

def hls_to_rgb(hue, light, saturation):
    def hls_to_rgb1(rn1, rn2, huei):
        hue = huei
        if hue > 360:
            hue = hue - 360
        elif hue < 0:
            hue = hue + 360
            
        if hue < 60:
            return rn1 + (rn2-rn1)*hue/60
        if hue < 180:
            return rn2
        if hue < 240:
            return rn1 + (rn2-rn1)*(240-hue)/60
        
        return rn1

    rh = max(min(hue, 360), 0)
    rl = max(min(light, 1), 0)
    rs = max(min(saturation, 1), 0)
 
    if rs == 0:
        return (rl, rl, rl)

    if rl <= 0.5:
       rm2 = rl*(1.0 + rs)
    else:
       rm2 = rl + rs - rl*rs
    rm1 = 2.0*rl - rm2
    
    return (
        hls_to_rgb1(rm1, rm2, rh+120),
        hls_to_rgb1(rm1, rm2, rh),
        hls_to_rgb1(rm1, rm2, rh-120)
        )


def rgb_to_hls(r, g, b):
    r = max(min(r, 1), 0)
    g = max(min(g, 1), 0)
    b = max(min(g, 1), 0)
 
    minval = min(r, g, b)
    maxval = max(r, g, b)
 
    mdiff = maxval - minval
    msum  = maxval + minval
    light = 0.5 * msum
    if maxval != minval:
       rnorm = (maxval - r)/mdiff
       gnorm = (maxval - g)/mdiff
       bnorm = (maxval - b)/mdiff
    else:
       return (0, light, 0)
 
    if light < 0.5:
       satur = mdiff/msum
    else:
       satur = mdiff/(2.0 - msum)
 
    if r == maxval:
       hue = 60.0 * (6.0 + bnorm - gnorm)
    elif g == maxval:
       hue = 60.0 * (2.0 + rnorm - bnorm)
    else:
       hue = 60.0 * (4.0 + gnorm - rnorm)
 
    if hue > 360:
       hue = hue - 360

    return (hue, light, satur)


def lighter(p1, g=None, b=None, power=1):
    if (g is None and b is not None) or (g is not None and b is None):
        raise RuntimeError('Invalid arguments.')
    
    if g is None and b is None:
        if not isinstance(p1, int):
            raise RuntimeError('ROOT color number must be an integer')
        color = _R.gROOT.GetColor(p1)
        h, l, s = (color.GetHue(), color.GetLight(), color.GetSaturation())
    else:
        h, l, s = rgb_to_hls(_float_rgb(p1), _float_rgb(g), _float_rgb(b))
        
    r, g, b = hls_to_rgb(h, min(1., (1.2**power)*l), s)
    return _R.TColor.GetColor(r, g, b)
