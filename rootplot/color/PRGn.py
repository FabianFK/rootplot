from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (118,42,131),
    (153,112,171),
    (194,165,207),
    (231,212,232),
    (247,247,247),
    (217,240,211),
    (172,211,158),
    (90,174,97),
    (27,120,55)
    ]

names = [ 'purple1', 'purple2', 'purple3', 'palepurple', 'lightgrey', 'palegreen', 'green3', 'green2', 'green1' ]

generate_colors(
    locals(),
    dict(list(zip(names, rgbs)) + [('paleyellow', (255,238,153))]),
    order = names
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

_palette_start = None
def set_palette():
    global _palette_start
    _palette_start = _set_palette(_palette_start, rgbs, 255)

del generate_colors
