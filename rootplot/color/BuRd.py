from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (33,102,172),
    (67,147,195),
    (146,197,222),
    (146,197,222),
    (247,247,247),
    (253,219,199),
    (244,165,130),
    (214,96,77),
    (178,24,43)
    ]

names = [ 'blue1', 'blue2', 'blue3', 'paleblue', 'lightgrey', 'palered', 'red3', 'red2', 'red1' ]

generate_colors(
    locals(),
    dict(list(zip(names, rgbs)) + [('paleyellow', (255,238,153))]),
    order = names
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

_palette_start = None
def set_palette():
    global _palette_start
    _palette_start = _set_palette(_palette_start, rgbs, 255)

del generate_colors
