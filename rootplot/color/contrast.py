from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'white': (255,255,255),
        'yellow': (221,170,51),
        'red': (187,85,102),
        'blue': (0,68,136),
        'black': (0,0,0)
    },
    order=['blue', 'yellow', 'red', 'black']
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

del generate_colors
