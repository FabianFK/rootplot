from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'blue': (34,34,85),
        'cyan': (34,85,85),
        'green': (34,85,34),
        'yellow': (102,102,51),
        'red': (102,51,51),
        'grey': (85,85,85)
    },
    order=['blue', 'cyan', 'green', 'yellow', 'red', 'grey']
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

del generate_colors
