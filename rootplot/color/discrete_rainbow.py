from ._make_color import generate_colors
from ._apply_color import apply_color

import ROOT

rgbs = [
    (232,236,251),
    (217,204,227),
    (209,187,215),
    (202,172,203),
    (186,141,180),
    (174,118,163),
    (170,111,158),
    (153,79,136),
    (136,46,114),
    (25,101,176),
    (67,125,191),
    (82,137,199),
    (97,149,207),
    (123,175,222),
    (78,178,101),
    (144,201,135),
    (202,224,171),
    (247,240,86),
    (247,203,69),
    (246,193,65),
    (244,167,54),
    (241,147,45),
    (238,128,38),
    (232,96,28),
    (230,85,24),
    (220,5,12),
    (165,23,14),
    (114,25,14),
    (66,21,10)
    ]

subsets = [
    [9],
    [9, 25],
    [9, 17, 25],
    [9, 14, 17, 25],
    [9, 13, 14, 17, 25],
    [9, 13, 14, 16, 17, 25],
    [8, 9, 13, 14, 16, 17, 25],
    [8, 9, 13, 14, 16, 17, 22, 25],
    [8, 9, 13, 14, 16, 17, 22, 25, 27],
    [8, 9, 13, 14, 16, 17, 20, 23, 25, 27],
    [8, 9, 11, 13, 14, 16, 17, 20, 23, 25, 27],
    [2, 5, 8, 9, 11, 13, 14, 16, 17, 20, 23, 25],
    [2, 5, 8, 9, 11, 13, 14, 15, 16, 17, 20, 23, 25],
    [2, 5, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25],
    [2, 5, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25, 27],
    [2, 4, 6, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25, 27],
    [2, 4, 6, 7, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25, 27],
    [2, 4, 6, 7, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25, 26, 27],
    [1, 3, 4, 6, 7, 8, 9, 11, 13, 14, 15, 16, 17, 19, 21, 23, 25, 26, 27],
    [1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 19, 21, 23, 25, 26, 27],
    [1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 20, 22, 24, 25, 26, 27],
    [1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 20, 22, 24, 25, 26, 27, 28],
    [0, 1, 3, 4, 6, 7, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 20, 22, 24, 25, 26, 27, 28],
    ]

generate_colors(
    locals(),
    dict(
        [ ('c%d'%(i+1), rgb) for i, rgb in enumerate(rgbs) ] + \
        [ ('grey', (119,119,119)) ]
        ),
    order = [ 'c%d'%(i+1) for i in range(len(rgbs)) ]
    )

def get_subset(n):
    if n <= len(subsets):
        return [ stylecolors[i] for i in subsets[n-1] ]
    else:
        pal = subsets[-1]
        nc = len(pal)
        return [ stylecolors[pal[i % nc]] for i in range(n) ]

def apply(*a):
    if len(a) == 0:
        return

    if len(a) == 1 and isinstance(a[0], list):
        objs = a[0]
    elif len(a) == 1 and isinstance(a[0], ROOT.TMultiGraph):
        objs = [ g for g in a[0].GetListOfGraphs() ]
    else:
        objs = a
    
    palette = get_subset(len(objs))

    for c, obj in zip(palette, objs):
        apply_color(c, obj)

def set_palette():
    raise RuntimeError(
        "This is the discrete rainbow color scheme. For a color palette, use smooth_rainbow."
        )

pale = c1
purple = c9
blue = c10
cyan = c14
green = c15
yellow = c18
orange = c23
red = c26
brown = c29

del generate_colors
