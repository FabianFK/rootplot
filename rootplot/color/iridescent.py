from ._make_color import generate_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (254,251,233),
    (252,247,213),
    (245,243,193),
    (234,240,181),
    (221,236,191),
    (208,231,202),
    (194,227,210),
    (181,221,216),
    (168,216,220),
    (155,210,225),
    (141,203,228),
    (129,196,231),
    (123,188,231),
    (126,178,228),
    (136,165,221),
    (147,152,210),
    (155,138,196),
    (157,125,178),
    (154,112,158),
    (144,99,136),
    (128,87,112),
    (104,73,87),
    (70,53,58)
    ]

generate_colors(
    locals(),
    dict(
        [ ('c%d'%(i+1), rgb) for i, rgb in enumerate(rgbs) ] + \
        [ ('grey', (153,153,153)) ]
        ),
    order = [ 'c%d'%(i+1) for i in range(len(rgbs)) ]
    )

def apply(*a):
    raise RuntimeError(
        "This color theme is meant for color palettes."
    )

_palette_start = None
def set_palette():
    global _palette_start
    _palette_start = _set_palette(_palette_start, rgbs, 255)

del generate_colors
