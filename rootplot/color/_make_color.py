import ROOT as _R

_next = _R.TColor.GetFreeColorIndex

def float_rgb(x):
    if isinstance(x, int):
        return float(x)/255.
    else:
        return x


def make_color_i(r, g, b, a=1.):
    ci = _next()
    return (ci, _R.TColor(ci, float_rgb(r), float_rgb(g), float_rgb(b), '', a))

    
def make_color(r, g, b, a=1.):
    return make_color_i(r, g, b, a)[1]


def generate_colors(dictionary, colors, order=None):
    d = {
        '_colordefinition': colors,
        '_colororder': order
        }
    
    allcolors = []
    for name, definition in colors.items():
        d['_'+name] = make_color_i(definition[0], definition[1], definition[2])
        d[name] = d['_'+name][0]
        allcolors.append(d[name])
    d['colors'] = allcolors

    if order is not None:
        d['stylecolors'] = [ d[name] for name in order ]
    else:
        d['stylecolors'] = allcolors

    dictionary.update(d)
