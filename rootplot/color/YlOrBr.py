from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (255,255,229),
    (255,247,188),
    (254,227,145),
    (254,196,79),
    (251,154,41),
    (236,112,20),
    (204,76,2),
    (153,52,4),
    (102,37,6)
    ]

names = [ '%s%d' % (n, i) for n in ['yellow', 'orange', 'brown'] for i in range(1,4) ]

generate_colors(
    locals(),
    dict(list(zip(names, rgbs)) + [('grey', (136,136,136))]),
    order = names
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

_palette_start = None
def set_palette():
    global _palette_start
    _palette_start = _set_palette(_palette_start, rgbs, 255)

del generate_colors
