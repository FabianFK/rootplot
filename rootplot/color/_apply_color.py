import numpy as _np
import ROOT as _R
from ._make_color import float_rgb


def apply_color(c, obj):
    if isinstance(obj, _R.TAttLine):
        obj.SetLineColor(c)
    if isinstance(obj, _R.TAttMarker):
        obj.SetMarkerColor(c)
    if isinstance(obj, _R.TArrow):
        obj.SetFillColor(c)


def apply_colors(colors, *a):
    if len(a) == 0:
        return
    
    if len(a) == 1 and isinstance(a[0], list):
        objs = a[0]
    elif len(a) == 1 and isinstance(a[0], _R.TMultiGraph):
        objs = [ g for g in a[0].GetListOfGraphs() ]
    else:
        objs = a

    if len(objs) <= len(colors):
        step = len(colors)/(len(objs)-1)
        for i, obj in enumerate(objs):
            idx = min(int(i*step), len(colors)-1)
            apply_color(colors[idx], obj)
    else:
        for i, obj in enumerate(objs):
            idx = i % len(colors)
            apply_color(colors[idx], obj)


def make_palette(rgbs, ncolors):
    red = _np.array([ float_rgb(x[0]) for x in rgbs ])
    green = _np.array([ float_rgb(x[1]) for x in rgbs ])
    blue = _np.array([ float_rgb(x[2]) for x in rgbs ])
    stops = _np.linspace(0., 1., len(rgbs))
    return _R.TColor.CreateGradientColorTable(len(rgbs), stops, red, green, blue, ncolors)


def set_palette(palette_start, rgbs, ncolors):
    if palette_start is None:
        return make_palette(rgbs, ncolors)
    else:
        _R.gStyle.SetPalette(ncolors, _np.arange(palette_start, palette_start+ncolors, dtype=_np.int32))
        return palette_start
