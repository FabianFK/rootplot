from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'indigo': (51,34,136),
        'cyan': (136,204,238),
        'teal': (68,170,153),
        'green': (17,119,51),
        'olive': (153,153,51),
        'sand': (221,204,119),
        'rose': (204,102,119),
        'wine': (136,34,85),
        'purple': (170,68,153),
        'pale': (221,221,221)
    },
    order=['indigo', 'cyan', 'teal', 'green', 'olive', 'sand', 'rose', 'wine', 'purple', 'grey']
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

del generate_colors
