from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'blue': (187,204,238),
        'cyan': (204,238,255),
        'green': (204,221,170),
        'yellow': (238,238,187),
        'red': (255,204,204),
        'grey': (221,221,221)
    },
    order=['blue', 'cyan', 'green', 'yellow', 'red', 'grey']
    )

apply = lambda *a: _apply_colors(stylecolors, *a)

del generate_colors
