from ._make_color import generate_colors
from ._apply_color import set_palette as _set_palette

rgbs = [
    (232,236,251),
    (221,216,239),
    (209,193,225),
    (195,168,209),
    (181,143,194),
    (167,120,180),
    (155,98,167),
    (140,78,153),
    (111,76,155),
    (96,89,169),
    (85,104,184),
    (78,121,197),
    (77,138,198),
    (78,150,188),
    (84,158,179),
    (89,165,169),
    (96,171,158),
    (105,177,144),
    (119,183,125),
    (140,188,104),
    (166,190,84),
    (190,188,72),
    (209,181,65),
    (221,170,60),
    (228,156,57),
    (231,140,53),
    (230,121,50),
    (228,99,45),
    (223,72,40),
    (218,34,34),
    (184,34,30),
    (149,33,27),
    (114,30,23),
    (82,26,19)
    ]

generate_colors(
    locals(),
    dict(
        [ ('c%d'%(i+1), rgb) for i, rgb in enumerate(rgbs) ] + \
        [ ('grey', (102,102,102)) ]
        ),
    order = [ 'c%d'%(i+1) for i in range(len(rgbs)) ]
    )

def apply(*a):
    raise RuntimeError(
        "This is the smooth rainbow color scheme for palettes. Use discrete_rainbow to color individual objects."
    )

_palette_start = {
    'full': None,
    'low': None,
    'high': None,
    'mid': None
    }
_begin = {
    'full': 0,
    'low': 0,
    'high': 8,   # purple
    'mid': 8
    }
_end = {
    'full': len(rgbs),
    'low': 30,  # red+1
    'high': len(rgbs),
    'mid': 30
    }
def set_palette(palette='full'):
    global _palette_start

    if palette not in _palette_start:
        raise RuntimeError("Unknown smooth_rainbow palette type: " + palette)
    
    _palette_start[palette] = _set_palette(
        _palette_start[palette],
        rgbs[_begin[palette]:_end[palette]],
        255
        )

del generate_colors
