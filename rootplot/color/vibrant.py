from ._make_color import generate_colors
from ._apply_color import apply_colors as _apply_colors

generate_colors(
    locals(),
    {
        'blue': (0,119,187),
        'cyan': (51,187,238),
        'teal': (0,153,136),
        'orange': (238,119,51),
        'red': (204,51,17),
        'magenta': (238,51,119),
        'grey': (187,187,187)
    },
    order=['blue', 'cyan', 'teal', 'orange', 'red', 'magenta', 'grey']
    )

apply = lambda *a: _apply_colors(stylecolors[:-1], *a)

del generate_colors
