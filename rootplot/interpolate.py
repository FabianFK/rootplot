import numpy as np
import scipy as sp
import scipy.interpolate

def loglog_interp1d(xx, yy, kind='linear', fill_value=np.nan):
    """Interpolate data on a double logarithmic scale. Returns a callable
    object that calculates the interpolated value at given x-value.
    
    Options:
    - `kind` passed on to `scipy.interpolate.interp1d`, which is applied
      to the logarithm of both `x` and `y` data. Leave at the default
      ('linear').
    - `fill_value` is passed on to `scipy.interpolate.interp1d` and
      can be used to determine if data should be extrapolated. To allow
      extrapolation beyond the data range, set to `'extrapolate'`."""
    logx = np.log10(xx)
    logy = np.log10(yy)
    lin_interp = sp.interpolate.interp1d(logx, logy, kind=kind, fill_value=fill_value)
    log_interp = lambda zz: np.power(10.0, lin_interp(np.log10(zz)))
    return log_interp

def semilog_interp1d(xx, yy, kind='linear', fill_value=np.nan):
    """Interpolate data on a logarithmic y-scale. Returns a callable
    object that calculates the interpolated value at given x-value.
    
    Options:
    - `kind` passed on to `scipy.interpolate.interp1d`, which is applied
      to the logarithm of the `y` data. Leave at the default ('linear').
    - `fill_value` is passed on to `scipy.interpolate.interp1d` and
      can be used to determine if data should be extrapolated. To allow
      extrapolation beyond the data range, set to `'extrapolate'`."""
    logy = np.log10(yy)
    lin_interp = sp.interpolate.interp1d(xx, logy, kind=kind, fill_value=fill_value)
    log_interp = lambda zz: np.power(10.0, lin_interp(zz))
    return log_interp

def logx_interp1d(xx, yy, kind='linear', fill_value=np.nan):
    """Interpolate data on a logarithmic x-scale. Returns a callable
    object that calculates the interpolated value at given x-value.
    
    Options:
    - `kind`passed on to `scipy.interpolate.interp1d`, which is applied
      to the logarithm of the `y` data. Leave at the default ('linear').
    - `fill_value` is passed on to `scipy.interpolate.interp1d` and
      can be used to determine if data should be extrapolated. To allow
      extrapolation beyond the data range, set to `'extrapolate'`."""
    logx = np.log10(xx)
    lin_interp = sp.interpolate.interp1d(logx, yy, kind=kind, fill_value=fill_value)
    log_interp = lambda zz: lin_interp(np.log10(zz))
    return log_interp
