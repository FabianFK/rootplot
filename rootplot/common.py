from .graphics import make_hist, print_label, make_gaxis, make_legend
from .canvas import make_canvas, split_canvas
