import ROOT
import numpy as np
from .core import rm

from builtins import str

    
def make_hist(name, bins=None,
              title='',
              labels=None,
              color=None,
              fillcolor=None,
              fontsize=0.05,
              linewidth=None,
              options=None,
              bindef=None):
    if isinstance(bins, ROOT.TH1):
        if name == bins.GetName():
            raise RuntimeError('Trying to replace histogram that provides binning (%s)' % name)

    rm(name)
    
    def _make_log_bins(n, xmin, xmax):
        xmin = np.log10(xmin)
        xmax = np.log10(xmax)
        dx = (xmax-xmin)/n
        return 10**np.arange(xmin, xmax+0.5*dx, dx)   # Add half a step to make sure the end point is included
    
    def _make_lin_bins(n, xmin, xmax):
        dx = (xmax-xmin)/n
        xbins = np.arange(xmin, xmax+0.5*dx, dx, dtype=float)   # Add half a step to make sure the end point is included
        
    def _make_bins(n, xmin, xmax, log):
        if log:
            return _make_log_bins(n, xmin, xmax)
        else:
            return _make_lin_bins(n, xmin, xmax)

    hist = None
    if isinstance(bins, ROOT.TH1):
        hist = bins.Clone(name)
        hist.Reset()
        hist.SetTitle(title)
        if hist.GetDimension() == 2:
            hist.SetContour(255)   # For some reason that info is lost.
    elif isinstance(bins, np.ndarray):
        hist = ROOT.TH1D(name, title, len(bins)-1, bins)
    elif bins is not None:
        if len(bins) == 3:
            logx = (options is not None and 'logx' in options)
            if logx:
                xbins = _make_log_bins(*bins)
                hist = ROOT.TH1D(name, title, bins[0], xbins)
            else:
                hist = ROOT.TH1D(name, title, *bins)
                
        elif len(bins) == 6:
            logx = False
            logy = False
            if options is not None:
                logx = 'logx' in options
                logy = 'logy' in options
                
            if not (logx or logy):
                hist = ROOT.TH2D(name, title, *bins)
            else:
                xbins = _make_bins(*bins[:3], logx)
                ybins = _make_bins(*bins[3:], logy)
                hist = ROOT.TH2D(name, title, bins[0], xbins, bins[3], ybins)
                
            hist.SetContour(255)
            
        elif len(bins) == 9:
            logx = False
            logy = False
            logz = False
            if options is not None:
                logx = 'logx' in options
                logy = 'logy' in options
                logz = 'logz' in options
                
            if not (logx or logy or logz):
                hist = ROOT.TH3D(name, title, *bins)
            else:
                xbins = _make_bins(*bins[:3], logx)
                ybins = _make_bins(*bins[3:6], logy)
                zbins = _make_bins(*bins[6:], logz)
                hist = ROOT.TH3D(name, title, bins[0], xbins, bins[3], ybins, bins[6], zbins)
                
            hist.SetContour(255)
                
    elif bindef is not None:
        if len(bindef) == 2 and isinstance(bindef[0], np.ndarray) and isinstance(bindef[1], np.ndarray):
            hist = ROOT.TH2D(name, title, len(bindef[0])-1, bindef[0], len(bindef[1])-1, bindef[1])
        elif isinstance(bindef, np.ndarray) and len(bindef) > 1:
            hist = ROOT.TH1D(name, title, len(bindef)-1, bindef)
            
    if hist is None:
        raise RuntimeError('No suitable bin definition provided.')
                
    if options is not None and 'stats' in options:
        hist.SetStats(True)
    else:
        hist.SetStats(False)
    
    hist.GetXaxis().SetTitleSize(fontsize)
    hist.GetXaxis().SetLabelSize(fontsize)
    hist.GetXaxis().CenterTitle()
    hist.GetYaxis().SetTitleSize(fontsize)
    hist.GetYaxis().SetLabelSize(fontsize)
    hist.GetYaxis().CenterTitle()
    hist.GetZaxis().SetTitleSize(fontsize)
    hist.GetZaxis().SetLabelSize(fontsize)
    hist.GetZaxis().CenterTitle()
    
    if options is not None and 'sumw2' in options:
        hist.Sumw2()
        
    if color is not None:
        hist.SetLineColor(color)
        hist.SetMarkerColor(color)
        
    if fillcolor is not None:
        hist.SetFillColor(fillcolor)

    if linewidth is not None:
        hist.SetLineWidth(linewidth)
    elif options is not None and 'thick' in options:
        hist.SetLineWidth(2)

    if options is not None and 'fill' in options:
        hist.SetFillColor(ROOT.kBlue-9)

    if labels is not None:
        if isinstance(labels, (list, tuple)):
            if len(labels) > 0:
                hist.SetXTitle(labels[0])
            if len(labels) > 1:
                hist.SetYTitle(labels[1])
            if len(labels) > 2:
                hist.SetZTitle(labels[2])
            if len(labels) > 3:
                print('WARNING: More than 3 labels given!')
        if isinstance(labels, str):
            hist.SetXTitle(labels)
    
    return hist


def scale_labels(obj, factor):
    for axis in [obj.GetXaxis(), obj.GetYaxis(), obj.GetZaxis()]:
        if axis:
            axis.SetTitleSize(factor*axis.GetTitleSize())
            axis.SetLabelSize(factor*axis.GetLabelSize())


def print_label(text, position, align='bl', ndc=False, color=ROOT.kBlack, font=42, size=0.05):
    ltx = ROOT.TLatex()
    ltx.SetTextFont(font)
    ltx.SetTextSize(size)
    ltx.SetTextColor(color)
    ltx.SetNDC(ndc)
    
    align_strings_ = {
        'bl': 11,
        'lb': 11,
        'cl': 12,
        'lc': 12,
        'tl': 13,
        'lt': 13,
        'bc': 21,
        'cb': 21,
        'cc': 22,
        'tc': 23,
        'ct': 23,
        'br': 31,
        'rb': 31,
        'cr': 32,
        'rc': 32,
        'tr': 33,
        'rt': 33
    }
    if align in align_strings_:
        ltx.SetTextAlign(align_strings_[align])
        
    ltx.SetText(position[0], position[1], text)
    
    return ltx


def make_gaxis(position, axisrange, title = '', color = ROOT.kBlack, font=42, size=0.05, ndiv=510, options=''):
    axopt = ''
    
    if 'right' in options:
        axopt += '+L'
    elif 'top' in options:
        axopt += '-L'

    if 'log' in options:
        axopt += 'G'
    
    axis = ROOT.TGaxis(position[0], position[1], position[2], position[3], axisrange[0], axisrange[1], ndiv, axopt)
    axis.SetLabelFont(font)
    axis.SetLabelSize(size)
    axis.SetLabelColor(color)
    axis.SetTitleFont(font)
    axis.SetTitleSize(size)
    axis.SetTitleColor(color)
    axis.SetLineColor(color)
    axis.SetTitle(title)
    axis.CenterTitle()
    
    return axis


def make_graph_axes(graph, xtitle = '', ytitle = '', xlimits=None, ylimits=None):
    def format_axis(axis, title):
        if axis:
            axis.SetTitle(title)
            axis.CenterTitle()
            axis.SetTitleSize(0.05)
            axis.SetLabelSize(0.05)
    
    format_axis(graph.GetXaxis(), xtitle)
    format_axis(graph.GetYaxis(), ytitle)
    
    if xlimits is not None:
        if graph.GetXaxis():
            graph.GetXaxis().SetLimits(xlimits[0], xlimits[1])
        
    if ylimits is not None:
        if graph.GetYaxis():
            graph.GetYaxis().SetRangeUser(ylimits[0], ylimits[1])


def make_legend(x1, y1, x2, y2, border=False, title='', font=42, size=0.05, titlefont=None):
    legend = ROOT.TLegend(x1, y1, x2, y2)
    legend.SetName('legend')
    if not border:
        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
    legend.SetTextSize(size)
    legend.SetTextFont(font)

    if len(title) > 0:
        if titlefont is not None:
            legend.SetHeader('#font[%d]{%s}' % (titlefont, title))
        else:
            legend.SetHeader(title)
    
    return legend
