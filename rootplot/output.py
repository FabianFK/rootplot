from .canvas import SmartCanvas

        
def set_path(path):
    import os
    SmartCanvas.PlotsPath = path
    try:
        os.makedirs(path)
    except os.error:
        pass


def path():
    return SmartCanvas.PlotsPath


def save_plots(do_save = True):
    SmartCanvas.SavePlots = do_save


def should_save_plots():
    return SmartCanvas.SavePlots
