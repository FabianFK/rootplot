import ROOT
import numpy as np
from .core import rm


class SmartCanvas(ROOT.TCanvas):
    PlotsPath = '.'
    SavePlots = True
    
    def __init__(self, size):
        super(SmartCanvas, self).__init__('c1', '', size[0], size[1])
        self.font_scale = 1.
        self.size = size

    def split(self, ratio):
        self.Modified()
        self.Update()
        
        lm = self.GetLeftMargin()
        rm = self.GetRightMargin()
        bp = self.GetBottomMargin() * self.size[1]  # bottom margin in pixels
        tp = self.GetTopMargin() * self.size[1]     # top margin in pixels
        fp = self.size[1] - tp - bp                 # frame height in pixels

        new_height = tp + (1 + 1./ratio)*fp + bp    # new canvas height in pixels

        dh = (bp + fp/ratio)/new_height             # fractional height of the bottom pad

        bm = bp/(bp + fp/ratio)
        tm = tp/(tp + fp)

        self.SetCanvasSize(self.size[0], int(new_height))
        self.SetMargin(0, 0, 0, 0)

        self.Divide(1, 2, 0, 0)

        pad = self.cd(1)
        pad.SetPad(0, dh, 1, 1)
        pad = self.cd(1)
        #pad.SetFillColor(R.kGreen)
        pad.SetMargin(lm, rm, 0, tm)
        pad.SetTickx()
        pad.SetTicky()

        pad = self.cd(2)
        pad.SetPad(0, 0, 1, dh)
        pad = self.cd(2)
        #pad.SetFillColor(R.kYellow)
        pad.SetMargin(lm, rm, bm, 0)
        pad.SetTickx()
        pad.SetTicky()

        self.font_scale = (1.-dh)/dh

        self.cd(1)
        
    def save_as(self, filename, imageformat='eps'):
        if not SmartCanvas.SavePlots:
            return
        
        super(SmartCanvas, self).SaveAs(SmartCanvas.PlotsPath + '/' + filename + '.root')
        if isinstance(imageformat, list):
            for fmt in imageformat:
                super(SmartCanvas, self).SaveAs(SmartCanvas.PlotsPath + '/' + filename + '.' + fmt)
        else:
            super(SmartCanvas, self).SaveAs(SmartCanvas.PlotsPath + '/' + filename + '.' + imageformat)


def make_canvas(size=(ROOT.gStyle.GetCanvasDefW(), ROOT.gStyle.GetCanvasDefH()),
                margins=(0.1, 0.1, 0.1, 0.1),
                options=None):
    rm('c1')
    c = SmartCanvas(size)
    c.SetMargin(margins[0], margins[1], margins[2], margins[3])

    if options is not None:
        if 'logx' in options:
            c.SetLogx()
        if 'logy' in options:
            c.SetLogy()
        if 'logz' in options:
            c.SetLogz()
        if 'loglog' in options:
            c.SetLogx()
            c.SetLogy()
    
    c.cd()
    return c


def scaled_size(sw, sh):
    return (int(sw*ROOT.gStyle.GetCanvasDefW()), int(sh*ROOT.gStyle.GetCanvasDefH()))


def split_canvas(canvas, ratio):
    canvas.split(ratio)
