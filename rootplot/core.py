import ROOT


def rm(name):
    try:
        obj = ROOT.__getattr__(name)
    except AttributeError:
        #print 'ERROR: %s not found.' % name
        return
    
    if isinstance(obj, ROOT.TCanvas):
        obj.Close()
        
    elif isinstance(obj, ROOT.TH1) or isinstance(obj, ROOT.TGraph):
        obj.Delete()

    else:
        obj.Delete()
